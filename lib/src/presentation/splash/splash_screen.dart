import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<SplashViewModel>(
        viewModel: SplashViewModel(),
        onViewModelReady: (viewModel) {
          viewModel.init();
        },
        builder: (context, viewModel, child) {
          return Scaffold(
            body: buildBody(),
          );
        });
  }

  Widget buildBody() {
    return Center(
      child: Image.asset(
        AppImages.png('app_logo'),
        scale: 2.5,
      ),
    );
  }
}
