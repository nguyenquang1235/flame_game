class AppImages {
  AppImages._();

  static final String imgLogo = 'assets/images/ic_moon.png';

  static String svg(String name) => 'assets/images/svg/$name.svg';
  static String png(String name) => 'assets/images/png/$name.png';
  static String jpg(String name) => 'assets/images/jpg/$name.jpg';
}
